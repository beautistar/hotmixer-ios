//
//  Const.swift
//  HotMixer
//
//  Created by JIS on 5/15/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

struct Const {
    
    static let APP_URL = "https://itunes.apple.com/us/app/riparadise/id1222792088?ls=1&mt=8"
    
    static let SAVE_ROOT_PATH = "Riparadiese"
    
    static let BASE_URL = "http://54.71.163.255/index.php/Api/"
    
    static let REQ_REGISTER = BASE_URL + "register/"
    static let REQ_LOGIN = BASE_URL + "login/"
    static let REQ_RESETPWD = BASE_URL + "resetPassword/"
    static let REQ_POST = BASE_URL + "post/"
    static let REQ_NEWSFEED = BASE_URL + "newsfeed/"
    static let REQ_GETCOMMENT = BASE_URL + "getComment/"
    static let REQ_SETCOMMENT = BASE_URL + "setComment/"
    static let REQ_SETLIKE = BASE_URL + "setLike/"
    static let REQ_SETSHARE = BASE_URL + "setShare/"
    
    
    static let RES_RESULTCODE = "result_code"
    static let RES_ID = "id"
    static let RES_PHOTOURL = "photo_url"
    static let RES_EMAIL = "email"
    static let RES_BIRTHDAY = "birthday"
    static let RES_USERNAME = "username"
    
    static let RES_NEWSFEED = "newsfeed"
    static let RES_USERID = "user_id"
    static let RES_CAPTION = "caption"
    static let RES_QUESTION1 = "question1"
    static let RES_QUESTION2 = "question2"
    static let RES_FILEURL = "file_url"
    
    static let RES_COMMENTS = "comments"
    static let RES_COMMENT = "comment"
    static let RES_REGDATE = "reg_date"
    static let RES_SHARECOUNT = "share_count"
    static let RES_COMMENTCOUNT = "comment_count"
    static let RES_LIKECOUNT = "like_count"
    static let RES_MYLIKETYPE = "my_like_type"
    static let RES_RECENTLIKETYPE = "recent_like_type"
    
    
    static let EMAIL = "email"
    static let PASSWORD = "password"
    
    static let borderColor = UIColor.init(red: 173, green: 111, blue: 219)
    static let mainColor = UIColor.init(red: 120, green: 61, blue: 164)
    static let barTintColor = UIColor.init(red: 102, green: 44, blue: 145)
    static let toolbarTintColor = UIColor.init(red: 38, green: 49, blue: 60)
    static let mainFontColor = UIColor.init(red: 149, green: 167, blue: 190)
    
    
    static let grayColor = UIColor.init(red: 245, green: 245, blue: 245)
    static let darkGrayColor = UIColor.init(red: 200, green: 200, blue: 200)
    
   
    
    static let CODE_SUCESS = 0
    
}

