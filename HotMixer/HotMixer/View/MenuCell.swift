//
//  MenuCell.swift
//  HotMixer
//
//  Created by JIS on 5/16/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var imvMenuIcon: UIImageView!
    @IBOutlet weak var lblMenuName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ iconPath:String, menuName:String) {
        
        imvMenuIcon.image = UIImage(named: iconPath)
        lblMenuName.text = menuName
    }

}
