//
//  MenuViewController.swift
//  HotMixer
//
//  Created by JIS on 5/15/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case shop = 0
    case orders
    case wishlist
    case about
    case gallery
    case videos
    case contact
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class MenuViewController: UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var tblMenu: UITableView!
    var menuNames = ["SHOP", "ORDERS", "WISHLIST", "ABOUT", "GALLERY", "VIDEOS", "CONTACT"]
    
    var menuIcons = ["ic_menu_shop", "ic_menu_order", "ic_menu_wishlist", "ic_menu_about", "ic_menu_gallery", "ic_menu_video", "ic_menu_contact"]
    
    var shopViewController: UIViewController!
    var orderViewController: UIViewController!
    var wishlistViewController: UIViewController!
    var aboutViewController: UIViewController!
    var galleryViewController: UIViewController!
    var videoViewController: UIViewController!
    var contactViewController: UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    
    func initView() {
        
        //        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let shopViewController = storyboard.instantiateViewController(withIdentifier: "ShopViewController") as! ShopViewController
        self.shopViewController = UINavigationController(rootViewController: shopViewController)
        
        let orderViewController = storyboard.instantiateViewController(withIdentifier: "OrderViewController") as! OrderViewController
        self.orderViewController = UINavigationController(rootViewController: orderViewController)
//
//        let myJobsViewController = storyboard.instantiateViewController(withIdentifier: "MyJobViewController") as! MyJobViewController
//        myJobsViewController.delegate = self
//        self.myJobsViewController = UINavigationController(rootViewController: myJobsViewController)
//        
//        let quoteRequestViewController = storyboard.instantiateViewController(withIdentifier: "QuoteRequestViewController") as! QuoteRequestViewController
//        quoteRequestViewController.delegate = self
//        self.quoteRequestViewController = UINavigationController(rootViewController: quoteRequestViewController)
//        
//        let myClientViewController = storyboard.instantiateViewController(withIdentifier: "MyClientViewController") as! MyClientViewController
//        myClientViewController.delegate = self
//        self.myClientViewController = UINavigationController(rootViewController: myClientViewController)
//        
//        let mainTabViewController = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
//        self.mainViewController = UINavigationController(rootViewController: mainTabViewController)
    }
    

    
    func changeViewController(_ menu: LeftMenu) {
        
        print(menu);
        
        switch menu {
            
        case .shop:
            self.slideMenuController()?.changeMainViewController(self.shopViewController, close: true)
            
        case .orders:
            self.slideMenuController()?.changeMainViewController(self.orderViewController, close: true)

//        case .quote_request:
//            self.slideMenuController()?.changeMainViewController(self.quoteRequestViewController, close: true)
//            
//        case .my_client:
//            self.slideMenuController()?.changeMainViewController(self.myClientViewController, close: true)
//        case .my_quote :
//            self.slideMenuController()?.changeMainViewController(self.myQuoteViewController, close: true)
//        case .my_jobs :
//            self.slideMenuController()?.changeMainViewController(self.myJobsViewController, close: true)
//            
//        case .invite_friend :
//            self.perform(#selector(showCustomDialog(_:)), with: nil, afterDelay: 0.1)
//            
//        case .callback_request :
//            self.perform(#selector(showCallBackReqDialig(_:)), with: nil, afterDelay: 0.1)
//            
//        case .site_visit_request :
//            self.perform(#selector(showVisitReqDialig(_:)), with: nil, afterDelay: 0.1)
//            
//        case .calendar_booking_requests:
//            self.perform(#selector(showCalendarBookingReqDialig(_:)), with: nil, afterDelay: 0.1)
            
            
        default :
            self.slideMenuController()?.changeMainViewController(self.shopViewController, close: true)
            
        }
    }

}

extension MenuViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblMenu == scrollView {
            
        }
    }
}

extension MenuViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        
        cell.setData(menuIcons[indexPath.row], menuName: menuNames[indexPath.row])
        return cell
    }
    
}
