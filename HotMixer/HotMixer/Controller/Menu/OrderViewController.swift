//
//  OrderViewController.swift
//  HotMixer
//
//  Created by JIS on 5/16/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import CarbonKit

class OrderViewController: BaseViewController, CarbonTabSwipeNavigationDelegate {
    
    var items = NSArray()
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setNavigationBarItem()
        
        //self.title = "CarbonKit"
        items = ["Active", "History"]
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self )
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        self.navigationItem.title = "ORDERS"
        
        let btnSetting = UIBarButtonItem(image: UIImage(named: "icon_setting"), style: .plain, target: self, action: #selector(ShopViewController.gotoSetting)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = btnSetting
        
        //let color: UIColor = UIColor(red: 24.0 / 255, green: 75.0 / 255, blue: 152.0 / 255, alpha: 1)
        
        let itemWidth = self.view.frame.size.width / CGFloat(items.count)
        
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = Const.mainColor
        self.navigationController!.navigationBar.barStyle = .blackTranslucent
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        carbonTabSwipeNavigation.toolbar.barTintColor = Const.toolbarTintColor
        carbonTabSwipeNavigation.setIndicatorColor(Const.mainColor)
        carbonTabSwipeNavigation.setTabExtraWidth(30)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(itemWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(itemWidth, forSegmentAt: 1)

        
        carbonTabSwipeNavigation.setNormalColor(UIColor.white.withAlphaComponent(0.6))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.white, font: UIFont.systemFont(ofSize: 14))
        
    }
    
    func gotoSetting() {
        
        print("goto setting")
    }

    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        switch index {
        case 0:
            return self.storyboard!.instantiateViewController(withIdentifier: "ActiveOrderViewController") as! ActiveOrderViewController
        case 1:
            return self.storyboard!.instantiateViewController(withIdentifier: "ActiveOrderViewController") as! ActiveOrderViewController
        default:
            return self.storyboard!.instantiateViewController(withIdentifier: "ActiveOrderViewController") as! ActiveOrderViewController
        }
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        NSLog("Did move at index: %ld", index)
    }

}
